# BuildSmart Api Proxy

Nginx proxy for the BuildSmart static files

# ENV

LISTEN_PORT... port that Nginx is listening on
APP_HOST... hostname of the app we're forwarding requests to
APP_PORT... port of the app which we're forwarding requests to
