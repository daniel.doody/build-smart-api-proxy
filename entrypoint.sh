#!/bin/sh

#during execution, if any lines fail return an err
set -e

# pass env to default location where nginx expects to find config file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# start nginx service
nginx -g 'daemon off;'